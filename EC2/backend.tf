terraform {
  backend "s3" {
    bucket         = "iac-tf-ec2-originapp" // S3 bucket for backend state
    key            = "terraform.tfstate" // path inside the bucket for storing state
    region         = "us-east-1" // region
    encrypt        = true
    dynamodb_table = "iac-originapp-ip" //  DynamoDB table name for state locking
  }
}

#test9
