terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}


#EC2 Instance
resource "aws_instance" "OriginServer" {
  instance_type = var.instance_type
  ami = var.ami
  user_data = file("userdata.sh")
  vpc_security_group_ids = [aws_security_group.OriginSg.id]
  key_name = "origin"

  tags = {
    Name = "OriginServer"
  }
}

#Networking
resource "aws_security_group" "OriginSg" {
  name        = "OriginSg"
  description = "Security group for web app"

 # Ingress rule for HTTP (port 80)
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

    # Ingress rule for HTTPS (port 443)
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Ingress rule for SSH (port 22)
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


    ingress {
    from_port   = 5001
    to_port     = 5001
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  # Allow traffic from any IP address
  }

  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }

  tags = {
    Name = "OriginSg"
  }
}

#ElasticIp Associateion
resource "aws_eip" "app_server_eip" {
  instance = aws_instance.OriginServer.id
}

resource "aws_eip_association" "app_server_eip_association" {
  instance_id   = aws_instance.OriginServer.id
  allocation_id = aws_eip.app_server_eip.id
}

# Outputs
output "public_ip" {
  value = aws_eip.app_server_eip.public_ip
}

output "ec2_dns" {
  value = aws_instance.OriginServer.public_dns
}

output "ec2_ssh_info" {
  value = "${aws_instance.OriginServer.public_dns}"
}

#test11