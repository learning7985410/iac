#!/bin/bash

# Update package index and install Docker
apt-get update
apt-get install -y docker.io

# add the current user to the docker group and start a new shell with the updated group membership.
sudo usermod -aG docker $USER && newgrp docker
sudo usermod -aG docker ubuntu && newgrp docker
