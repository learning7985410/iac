variable "instance_type" {
  description = "The type of EC2 instance to launch"
  default     = "t2.micro"
}

variable "ami" {
  description = "The type of UBUNTU EC2 AMI to be used"
  default     = "ami-07d9b9ddc6cd8dd30"
}


#test13