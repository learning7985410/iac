terraform {
  backend "gcs" {
    bucket  = "gcp_state_tf"
    prefix  = "terraform/state"
  }
}
