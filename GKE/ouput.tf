output "cluster_name" {
  value = google_container_cluster.gke_cluster.name
}

output "cluster_endpoint" {
  value = google_container_cluster.gke_cluster.endpoint
}

output "gke_cluster_zone" {
  value = var.region
}

output "gke_project_id" {
  value = var.project_id
}

#test1