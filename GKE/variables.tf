variable "project_id" {
  description = "The ID of the GCP project"
}

variable "region" {
  description = "The region where the GKE cluster will be created"
  default     = "us-west1"
}

variable "cluster_name" {
  description = "The name of the GKE cluster"
  default     = "origin-gke-cluster"
}

variable "node_count" {
  description = "The number of nodes in the node pool"
  default     = 1
}

variable "machine_type" {
  description = "The machine type for the nodes in the node pool"
  default     = "e2-medium"
}

variable "service_account_key_file" {
  description = "The path to the service account key file"
}

#test8
