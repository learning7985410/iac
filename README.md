# Infrastructure as Code (IaC) Repository

## Refer below PDF for Detailed Documentation 
- https://gitlab.com/learning7985410/iac/-/blob/main/Detailed_Documentation_Take-Home_Assignment__DevSecOps_Engineerpdf.pdf?ref_type=heads 

## Links:

**Docker Hub :**
- https://hub.docker.com/repository/docker/secenthu/origin
- Image Path : secenthu/origin:{$Commit_ID}

**Application Links:**

- EC2 Application : http://ec2-34-225-114-20.compute-1.amazonaws.com:5001/
- GKE Application: http://35.185.238.176/

**Repository Links:**

- Iac Repo : https://gitlab.com/learning7985410/iac
- OriginIp Repo: https://gitlab.com/learning7985410/origin-ip


## Overview

This repository contains the Terraform configurations for provisioning and managing cloud infrastructure on AWS and GCP. It includes configurations for creating a GKE cluster, EC2 instances, security groups, and other necessary infrastructure components.

## Prerequisites

- Access to an AWS account with necessary permissions.
- Access to a GCP account with necessary permissions.
- Properly configured AWS CLI and Google Cloud SDK.

## Directory Structure
 - GKE/: Contains Terraform configurations for provisioning a Google Kubernetes Engine (GKE) cluster.
 - EC2/: Contains Terraform configurations for provisioning AWS EC2 instances and associated resources.
 - .gitlab-ci.yml : This is used to implement CI/CD on the terraform code



## Continuous Integration (CI)
The repository is integrated with GitLab CI/CD to automatically validate, plan, and apply Terraform configurations upon code changes. The pipeline includes stages for:

Validation: Checks the syntax and validity of Terraform configurations. 
Planning: Generates an execution plan for Terraform.
Testing: Performs security scanning on the IaC files using tools like Checkov.
Applying: Applies the Terraform plan to provision or update the infrastructure.

## Security
Secret Detection: Scans the repository for any exposed secrets.
IaC Scanning: Uses Checkov to scan the Terraform files for security misconfigurations.



## Getting Started

1. **Clone the Repository**:

   ```bash
   git clone https://gitlab.com/learning7985410/iac.git
   cd iac

2. **Terraform Related**

**Initialize Terraform:**

Navigate to the desired environment directory (e.g., GKE or EC2) and run:

terraform init

**Create a Terraform Plan:**

terraform plan -out=tfplan

**Apply the Terraform Plan:**

terraform apply "tfplan"
